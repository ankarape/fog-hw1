# -*- coding: utf-8 -*-
import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

model_1 = np.load('Monkey.npy')

figure = plt.figure(1)
ax_1 = mplot3d.Axes3D(figure)
poly3d_1 = mplot3d.art3d.Poly3DCollection(model_1)
ax_1.add_collection3d(poly3d_1)

# a) limit the ranges of all three axes to [-2,...2]
ax_1.set_xlim([-2, 2])
ax_1.set_ylim([-2, 2])
ax_1.set_zlim([-2, 2])

# b) translate and scale the model
model_2 = np.multiply(model_1, 2) + [1, 0, 0]
poly3d_2 = mplot3d.art3d.Poly3DCollection(model_2)

# show results in a new plot
figure = plt.figure(2)
ax_2 = mplot3d.Axes3D(figure)
ax_2.set_xlim([-2,2])
ax_2.set_ylim([-2,2])
ax_2.set_zlim([-2,2])

ax_2.add_collection3d(poly3d_2)

# c) rotate model around z-axis and display both at the same time
# a.

figure = plt.figure(3)
ax_3 = mplot3d.Axes3D(figure)
ax_3.set_xlim([-2,2])
ax_3.set_ylim([-2,2])
ax_3.set_zlim([-2,2])

alpha = math.pi / 2
Rotation_matrix_1 = [[math.cos(alpha), - math.sin(alpha), 0],
                     [math.sin(alpha), math.cos(alpha), 0],
                     [0, 0, 1]]

rotated_model_1 = np.empty(model_1.shape)
for i in range(rotated_model_1.shape[0]):
    rotated_model_1[i] = np.dot(Rotation_matrix_1, model_1[i].T).T

poly3d_3 = mplot3d.art3d.Poly3DCollection(rotated_model_1)
ax_3.add_collection3d(poly3d_3)

# b.
alpha = math.pi / 2
Rotation_matrix_2 = [[math.cos(alpha), - math.sin(alpha), 0],
                     [math.sin(alpha), math.cos(alpha), 0],
                     [0, 0, 1]]

# change the coordinate system: (x, y, z) -> (x-1, y, z)
model_2_shifted = model_2 + [-1, 0, 0]

# apply the rotation 
rotated_model_2 = np.empty(model_2_shifted.shape)
for i in range(rotated_model_2.shape[0]):
    rotated_model_2[i] = np.dot(Rotation_matrix_2, model_2_shifted[i].T).T
    
# transform the coordinates back: (x, y, z) -> (x+1, y, z)
rotated_model_2 = rotated_model_2 + [1, 0, 0]

poly3d_4 = mplot3d.art3d.Poly3DCollection(rotated_model_2)
ax_3.add_collection3d(poly3d_4)


plt.show()